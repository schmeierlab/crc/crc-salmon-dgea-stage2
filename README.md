# DGEA from SALMON-based quasi-mappings

- AUTHOR: Sebastian Schmeier (s.schmeier@protonmail.com)
- DATE: 2019

## Purpose

This workflow allows to run a differential expression analysis, PCA, and gene
set enrichment analysis based on quantified transcript expressions from
[Salmon](https://combine-lab.github.io/salmon/).

The workflow expects that [Salmon](https://combine-lab.github.io/salmon/) has already been used to quantify transcripts. It will work of these quantifications.

## Prereqs

0. Install snakemake
1. Adjust the config file where necessary.
2. Run the workflow
3. Create report


### 0. Install snakemake and clone this repo

```bash
conda create -n snakemake snakemake --yes
conda activate snakemake
```

```bash
git clone git@gitlab.com:schmeierlab/workflows/ngs-quasimapping.git
# or 
git clone https://gitlab.com/schmeierlab/workflows/ngs-quasimapping.git
```


### 1. Adjust the config file where necessary

Point to the correct samplesheet, transcriptome index and annotation.
Change the contrasts if differential expression analysis should be performed.

Note: `--configfile <file>` needs to be specified as a `snakemake` argument.


### 2. Running the workflow

#### Singularity-only mode (recommended)

Change the `singularity` variable in the `config.yaml` to `singularity: shub://sschmeier/simg-rnaseq:201903`

```bash
snakemake -p --use-singularity --singularity-args "--bind /mnt/DATA/seb" --jobs 10 --configfile config.yaml 2> run.log
```

#### Singularity + Conda  mode

Change the `singularity` variable in the `config.yaml` to `singularity: docker://continuumio/miniconda3:4.5.12`

```bash
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt/DATA/seb" --jobs 10 --configfile config.yaml 2> run.log
```

#### Conda-only mode

```bash
snakemake -p --use-conda --jobs 10 --configfile config.yaml 2> run.log 
```


### 3. Create report

```bash
snakemake --configfile config.yaml --report report.html
```
