#!/usr/bin/env python
"""
NAME: test
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.3    201808  Removed the ref_gene_id extraction again.
0.0.2    201808  Get ref_gene_id if available instead of gene_id
0.0.1    2018    Initial version.

LICENCE
=======
2017, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time
import re

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.3'
__date__ = '20180806'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '%s [%s] %s\r'%(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '%s [%s] %s\n'%(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    
    log.write('%s%s%s'%(colors[atype], textout, reset))
    if atype=='error': sys.exit(1)

def success(text, log=sys.stderr):
    alert('success', text, log)

def error(text, log=sys.stderr):
    alert('error', text, log)

def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read gtf-file and extract tx to gene associations based on class codes for novels and genes. If a code is not in novels or genes, it is ignored. Extract tx to gene info from reference.'
    version = 'version %s, date %s'%(__version__, __date__)
    epilog = 'Copyright %s (%s)'%(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s'%(version))

    parser.add_argument(
        'str_file',
        metavar='FILE',
        help=
        'GTF reference file, e.g. from gencode.')
    parser.add_argument('-o',
                        '--out',
                        metavar='STRING',
                        dest='outfile_name',
                        default=None,
                        help='Out-file. [default: "stdout"]')
    parser.add_argument('--noversion',
                        action="store_true",
                        dest='noversion',
                        default=False,
                        help='Do not use gene versions. [default: "False"]')
    parser.add_argument('--element',
                        metavar='STRING',
                        dest='element',
                        default="transcript",
                        help='If column 3 of gtf is this element the line will be parsed. [default: "transcript"]')
    parser.add_argument('--gencode',
                        action="store_true",
                        dest='gencode',
                        default=False,
                        help='Use Gencode gtf-file format. [Ensembl is default]')
    parser.add_argument('--ncbi',
                        action="store_true",
                        dest='ncbi',
                        default=False,
                        help='Use NCBI gtf-file format. [Ensembl is default]')
    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        fileobj = load_file(args.str_file)
    except:
        error('Could not load file. EXIT.')

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ['-', 'stdout']:
        outfileobj = sys.stdout
    elif args.outfile_name.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.outfile_name, 'wt')
    else:
        outfileobj = open(args.outfile_name, 'w')

    # parsing anno
    if args.gencode:
        regRef = re.compile('gene_id\s+"(.+?)";.+transcript_id\s+"(.+?)";.+gene_name\s+"(.+?)"')
    elif args.ncbi:
        regRef = re.compile('gene_id\s+"(.+?)";.+transcript_id\s+"(.+?)";.+gene\s+"(.+?)"')
    else: # ensembl format
        regRef = re.compile('gene_id\s+"(.+?)";.+transcript_id\s+"(.+?)";.+transcript_version\s+"(.+?)";.+gene_name\s+"(.+?)"')
        
    d = {}
    for line in fileobj:
        if line[0] == "#":
            continue
        
        a = [s.strip() for s in line.split('\t')]
        if a[2] != args.element:
            continue

        res = regRef.search(line)
        if not res:
            continue
        else:
            if args.gencode:
                gene, tx, genename = res.groups()
            elif args.ncbi:
                gene, tx, genename = res.groups()
            else:  # ensembl
                gene, tx, version, genename = res.groups()

            if args.noversion:
                gene = gene.split('.')[0]

            if args.gencode:
                d[(genename, tx, gene, genename)] = None
            elif args.ncbi:
                d[(genename, tx, gene, genename)] = None
            else:  # ensembl
                d[(genename, '%s.%s'%(tx,version), gene, genename)] = None
            
    fileobj.close()      
    
    a = list(d.keys())
    if len(a) == 0:
        error("Could not extract any entries from the gtf-file. Check your annotation file.")
    a.sort()
    for tx in a:
        outfileobj.write('%s\t%s\t%s\n'%(tx[1], tx[2], tx[3]))


    # ------------------------------------------------------
    outfileobj.close()
    return


if __name__ == '__main__':
    sys.exit(main())

