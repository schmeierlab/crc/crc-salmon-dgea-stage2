#!/usr/bin/env python
"""

VERSION HISTORY

0.2.0   2014/07/18    Option to print not mathcing target lines
0.1.2   2014/02/26    Minor improvements.
0.1.1   2012/03/26    Minor fixes.
0.1     2012/03/25    Initial version.

"""
__version__='0.3.0'
__date__='2017/10/19'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, os, argparse, csv
import gzip, bz2, zipfile

def parse_cmdline():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Join fields from a reference file onto lines of a target-file based on a map on specific fields.' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)
    parser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))
    parser.add_argument('sFile',
                         type=str,
                         metavar='TARGETFILE',
                         help='Delimited file. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument('sRef',
                         type=str,
                         metavar='REFFILE',
                         help='Reference-file. Delimited file.')
    parser.add_argument('sStats',
                         type=str,
                         metavar='STATSFILE',
                         help='Expression-stats file. Delimited file.')
    parser.add_argument('--tx',
                         action='store_true',
                         dest='tx',
                         default=False,
                         help='Use tx ids from ref. [default: False]')
    parser.add_argument('--txversion',
                         action='store_true',
                         dest='txversion',
                         default=False,
                         help='Remove tx version from id. [default: False]')
    parser.add_argument('--annotationtype',
                        metavar='STRING',
                        dest='anno',
                        default="ensembl",
                        help='Gene/tx annotation type: ensembl, gencode, ncbi [default: "ensembl"]')
    
   
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    
    args = parser.parse_args()
    return args, parser

def main():
    args, parser = parse_cmdline()

    if args.sFile in ['-', 'stdin']:
        oF = sys.stdin
    if args.sFile.split('.')[-1] == 'gz':
        oF = gzip.open(args.sFile, 'rt')
    elif args.sFile.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(args.sFile)
    elif args.sFile.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(args.sFile)
    else:
        oF = open(args.sFile)
   
    if args.sRef.split('.')[-1] == 'gz':
        oFref = gzip.open(args.sRef, 'rt')
    elif args.sRef.split('.')[-1] == 'bz2':
        oFref = bz2.BZFile(args.sRef)
    elif args.sRef.split('.')[-1] == 'zip':
        oFref = zipfile.Zipfile(args.sRef)
    else:
        oFref = open(args.sRef)

    if args.sStats.split('.')[-1] == 'gz':
        oFstat = gzip.open(args.sStats, 'rt')
    elif args.sStats.split('.')[-1] == 'bz2':
        oFstat = bz2.BZFile(args.sStats)
    elif args.sStats.split('.')[-1] == 'zip':
        oFstat = zipfile.Zipfile(args.sStats)
    else:
        oFstat = open(args.sStats)
        
    # build reference
    oR = csv.reader(oFref, delimiter = "\t")
    dREF = {}
    for a in oR:
        if args.tx:
            dREF[a[0]] = a[2]
        else:
            dREF[a[1]] = a[2]

    oR = csv.reader(oF, delimiter = "\t")
    header_de = list(next(oR))
    dge = {}
    for a in oR:
        dge[a[0]] = a


    # stat file
    oR = csv.reader(oFstat, delimiter = "\t")
    header = header_de + ["GeneSymbol", "Link"] + list(next(oR))[1:]
    sys.stdout.write("{}\n".format("\t".join(header)))
    for a in oR:
        res = [a[0]]
        if a[0] not in dge:
            res += ["NA"]*7
        else:
            res += dge[a[0]][1:]

        # attach symbol and link
        if args.txversion:
            idx = a[0].split('.')[0]
        else:
            idx = a[0]
        
        if idx not in dREF:
            sys.stderr.write("Id {} not found in reference. Skip".format(idx))
            symbol = "NA"
        else:
            symbol = dREF[idx]
        res.append(symbol)
        
        if args.anno in ["ensembl" , "genecode"]:
            link = "http://identifiers.org/ENSEMBL:{}".format(idx.split(".")[0])
        elif args.anno == "ncbi":
            link = "http://identifiers.org/REFSEQ:{}".format(idx)
        else:
            link = ""
        res.append(link)
            
        # attach stats
        res += a[1:]
        
        sys.stdout.write("{}\n".format("\t".join(res)))
        
    return
        
if __name__ == '__main__':
    sys.exit(main())

