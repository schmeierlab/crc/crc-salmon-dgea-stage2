## Stage2

This is a DGEA based on 2017b quantified data.
This DGEA compares stage 2 patients that died vs alive patients.


```bash
snakemake -p --use-singularity --singularity-args "--bind /mnt/disk2 --bind /mnt/disk1" --jobs 6 --configfile config.crc-stage2.yaml 2> run.crc-stage2.lo
```