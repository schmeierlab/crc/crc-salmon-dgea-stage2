import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath, isdir
from snakemake.utils import validate, min_version

##### set minimum snakemake version #####
min_version("5.4.0")

## =============================================================================
## SETUP
## =============================================================================

# DIRS
DIR_SCHEMAS = abspath("schemas")
DIR_SCRIPTS = abspath("scripts")
DIR_ENVS    = abspath("envs")

##### load config and sample sheets #####
# This needs to be commented out for now as there is a bug
# https://bitbucket.org/snakemake/snakemake/issues/1184/configfile-does-not-replace-snakefile
# thats merges configs...
##configfile: "config.yaml"

if config=={}:
    print('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

# specify config file with --configfile
# validate config file
validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))

## outputs
DIR_BASE       = abspath(config["basedir"])
DIR_RES        = join(DIR_BASE, "results")
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")

## define global Singularity image for reproducibility
## USE: "--use-singularity --use-conda" to run all jobs in container
singularity: config["singularity"]

## sample inputs
CFG_SAMPLESHEET = abspath(config["samples"])
DIR_QUANTS = abspath(config["quants"])

## =============================================================================
## TOOL PARAMETERS

# GTF
CFG_GTF          = config["annotation"]["file"]
CFG_ELEMENT      = config["annotation"]["element"]
CFG_GTF_SOURCE   = config["annotation"]["source"]
if CFG_GTF_SOURCE == "gencode":
    CFG_SOURCE = "--gencode"
elif CFG_GTF_SOURCE == "ncbi":
    CFG_SOURCE = "--ncbi"
else:
    CFG_SOURCE = ""

# DGEA contrasts
CFG_CONTRASTS    = config["diffexp"]["contrasts"]

# GSEA
#CFG_ENRICH_ID    = config["gsea"]["idtype"]
#CFG_ENRICH_ORG   = config["gsea"]["organism"]
CFG_ENRICH_GMT   = config["gsea"]["gmt"]
CFG_ENRICH_GCOL  = config["gsea"]["genecol"]
CFG_GSEA_NPERMS  = config["gsea"]["nperms"]

## Which analyses to perform?
CFG_TODO         = config["todo"]

## =============================================================================
## LOAD SAMPLES, checkif files exist

# validate samplesheet
samples = pd.read_csv(CFG_SAMPLESHEET, sep="\t").set_index("sample", drop=False)
validate(samples, schema=join(DIR_SCHEMAS, "samples.schema.yaml"))

# test if sample in dir
for sample in samples["sample"]:
    if not isdir(join(DIR_QUANTS, sample)):
        sys.stderr.write("Dir '{}' cannot be found. Make sure the dir exists. Exit\n".format(join(DIR_QUANTS, sample)))
        sys.exit()

NUM_SAMPLES = len(samples["sample"])
sys.stderr.write('{} samples to process\n'.format(NUM_SAMPLES))

## =============================================================================
## FUNCTIONS
## =============================================================================
def get_contrast(wildcards):
    return config["diffexp"]["contrasts"][wildcards.contrast]


## =============================================================================
## SETUP TARGETS
## =============================================================================
TARGETS = [join(DIR_RES, "002_expr/tx.tpm.stats.txt.gz"),
           join(DIR_RES, "002_expr/tx.tpm.gct.gz"),
           join(DIR_RES, "002_expr/genes.tpm.stats.txt.gz"),
           join(DIR_RES, "002_expr/genes.tpm.gct.gz")]

if "diffexpr" in CFG_TODO:
    TARGETS += expand([join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
                       join(DIR_RES, "003_diffexp/{contrast}.genes.ma-plot.pdf"),
                       join(DIR_RES, "003_diffexp/{contrast}.tx.diffexp.stats.tsv.gz"),
                       join(DIR_RES, "003_diffexp/{contrast}.tx.ma-plot.pdf")],
                      contrast=CFG_CONTRASTS)
    
if "pca" in CFG_TODO:
    TARGETS += [join(DIR_RES, "pca.genes.pdf")]

if "gsea" in CFG_TODO:
    if "diffexpr" in CFG_TODO:
        TARGETS += expand([join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.all.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.all.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.selected.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.selected.tsv.gz")],
                          contrast=CFG_CONTRASTS)
    else:
        TARGETS += expand([join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.stats.tsv.gz"),
                           join(DIR_RES, "003_diffexp/{contrast}.genes.ma-plot.pdf"),
                           join(DIR_RES, "003_diffexp/{contrast}.tx.diffexp.stats.tsv.gz"),
                           join(DIR_RES, "003_diffexp/{contrast}.tx.ma-plot.pdf"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.all.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.all.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.selected.tsv.gz"),
                           join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.selected.tsv.gz")],
                          contrast=CFG_CONTRASTS)

## =============================================================================
## RULES
## =============================================================================
rule all:
    input:
        TARGETS


## ===================================================================
## EXPRESSION

# rule to create samplesheet with quant.sf path for tximport 
rule get_sample_quant_path:
    # we attach the path to the quant.sf file to the sample
    input:
        sheet=CFG_SAMPLESHEET,
        samples=expand(join(DIR_QUANTS, "{sample}/quant.sf"), sample=samples["sample"])
    output:
        join(DIR_RES, "sample_list.txt")
    log:
        join(DIR_LOGS, "get_sample_quant_path.stderr")
    params:
        script = join(DIR_SCRIPTS, "sample_list.py")
    shell:
        "python {params.script} {input.sheet} {DIR_QUANTS} > {output} 2> {log}"

        
# we create tx_gene_map from gtf annotation for tximport 
rule get_tx_gene_map:
    input:
        CFG_GTF
    output:
        join(DIR_RES, "tx_gene_map.txt")
    log:
        join(DIR_LOGS, "get_tx_gene_map.stderr")
    benchmark:
        join(DIR_BENCHMARKS,"get_tx_gene_map.txt")
    params:
        script = join(DIR_SCRIPTS, "extract_txmap_from_gtf.py"),
        extra="--noversion",
        source=CFG_SOURCE,
        elem=CFG_ELEMENT
    shell:
        "python {params.script} {params.extra} {params.source} --element {params.elem} {input} > {output} 2> {log}"


## rule to get the Deseq2 objects and raw and normalised counts
rule deseq2_init:
    input:
        samplelist=join(DIR_RES, "sample_list.txt"),
        t2g=join(DIR_RES, "tx_gene_map.txt")
    output:
        join(DIR_RES, "001_deseq2/dds.tx.rds"),
        join(DIR_RES, "002_expr/tx.counts.normalized.txt.gz"),
        join(DIR_RES, "002_expr/tx.counts.raw.txt.gz"),
        join(DIR_RES, "001_deseq2/dds.rds"),
        join(DIR_RES, "002_expr/genes.counts.normalized.txt.gz"),
        join(DIR_RES, "002_expr/genes.counts.raw.txt.gz")
    params:
        samples=config["samples"],
        paired=config["diffexp"]["paired"]
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "deseq2/init.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2_init.txt")
    threads: 6
    script:
        join(DIR_SCRIPTS, "deseq2-init.R")


## rule to get TPM expr
rule get_expr_tpm:
    input:
        samplelist=join(DIR_RES, "sample_list.txt"),
        t2g=join(DIR_RES, "tx_gene_map.txt")
    output:
        join(DIR_RES, "002_expr/tx.tpm.txt.gz"),
        join(DIR_RES, "002_expr/genes.tpm.txt.gz")
    log:
        join(DIR_LOGS, "get_expr_tpm.log")
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    threads: 6
    script:
        join(DIR_SCRIPTS, "get_expr_tpm.R")
        
        
## rule to make some stats for all tx
rule get_tx_stats:
    input:
        join(DIR_RES, "002_expr/tx.tpm.txt.gz"),
        CFG_SAMPLESHEET
    output:
        join(DIR_RES, "002_expr/tx.tpm.stats.txt.gz")
    log:
        join(DIR_LOGS, "get_tx_stats.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_tx_stats.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    params:
        script = join(DIR_SCRIPTS, "tx_stats.py")
    shell:
        "python {params.script} {input[0]} {input[1]} 2> {log} | gzip > {output}"

        
#rule to make some stats for all genes
rule get_gene_stats:
    input:
        join(DIR_RES, "002_expr/genes.tpm.txt.gz"),
        CFG_SAMPLESHEET
    output:
        join(DIR_RES, "002_expr/genes.tpm.stats.txt.gz")
    log:
        join(DIR_LOGS, "get_gene_stats.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gene_stats.txt")
    conda:
        join(DIR_ENVS, "pandas.yaml")
    params:
        script = join(DIR_SCRIPTS, "tx_stats.py")
    shell:
        "python {params.script} {input[0]} {input[1]} 2> {log} | gzip > {output}"

        
#rule to make gct files for igv visualisation
rule get_gct_tx:
    input:
        join(DIR_RES, "002_expr/tx.tpm.txt.gz"),
        CFG_GTF
    output:
        join(DIR_RES, "002_expr/tx.tpm.gct.gz")
    log:
        join(DIR_LOGS, "get_gct_tx.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gct_tx.txt")
    params:
        script = join(DIR_SCRIPTS, "build_gct.py"),
        extra = r''
    shell:
        "python {params.script} {params.extra} {input[0]} {input[1]} | gzip > {output} 2> {log}"

        
#rule to make gct files for igv visualisation
rule get_gct_gene:
    input:
        join(DIR_RES, "002_expr/genes.tpm.txt.gz"),
        CFG_GTF
    output:
        join(DIR_RES, "002_expr/genes.tpm.gct.gz")
    log:
        join(DIR_LOGS, "get_gct_gene.stderr")
    benchmark:
        join(DIR_BENCHMARKS, "get_gct_gene.txt")
    params:
        script = join(DIR_SCRIPTS, "build_gct.py"),
        extra = r'--gene --noversion'
    shell:
        "python {params.script} {params.extra} {input[0]} {input[1]} | gzip > {output} 2> {log}"


## ===================================================================
## pca
rule pca:
    input:
        join(DIR_RES, "002_deseq2/dds.tx.rds"),
        join(DIR_RES, "002_deseq2/dds.rds")
    output:
        join(DIR_RES, "pca.tx.pdf"),
        join(DIR_RES, "pca.genes.pdf")
    params:
        pca_labels=config["pca"]["labels"],
        function=config["pca"]["function"]
    conda:
        join(DIR_ENVS, "dexpr.yaml")
    log:
        join(DIR_LOGS, "pca.log")
    script:
        join(DIR_SCRIPTS, "plot-pca.R")


## ===================================================================
## diffexp
rule deseq2:
    input:
        join(DIR_RES, "001_deseq2/dds.rds")
    output:
        table=join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.tsv.gz"),
        ma_plot=join(DIR_RES, "003_diffexp/{contrast}.genes.ma-plot.pdf")
    log:
        join(DIR_LOGS, "deseq2/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2.{contrast}.txt")
    conda:
       join(DIR_ENVS, "dexpr.yaml")
    threads: 6
    params:
        contrast=get_contrast,
        filterFun=config["diffexp"]["filterFun"]
    script:
        join(DIR_SCRIPTS, "deseq2.R")

        
rule deseq2_tx:
    input:
        join(DIR_RES, "001_deseq2/dds.tx.rds")
    output:
        table=join(DIR_RES, "003_diffexp/{contrast}.tx.diffexp.tsv.gz"),
        ma_plot=join(DIR_RES, "003_diffexp/{contrast}.tx.ma-plot.pdf")
    log:
        join(DIR_LOGS, "deseq2.tx/{contrast}.diffexp.log")
    benchmark:
        join(DIR_BENCHMARKS, "deseq2_tx.{contrast}.txt")
    conda:
       join(DIR_ENVS, "dexpr.yaml")
    threads: 6
    params:
        contrast=get_contrast,
        filterFun=config["diffexp"]["filterFun"]
    script:
        join(DIR_SCRIPTS, "deseq2.R")


## join dge and stats
rule attach_genes_stats:
    input:
        de=join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.tsv.gz"),
        map=join(DIR_RES, "tx_gene_map.txt"),
        stats=join(DIR_RES, "002_expr/genes.tpm.stats.txt.gz")
    output:
        join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.stats.tsv.gz")
    log:
        join(DIR_LOGS, "attach_gene_symbol_{contrast}.log")
    params:
        script=join(DIR_SCRIPTS, "join.py"),
        anno=CFG_GTF_SOURCE
    shell:
        "python {params.script} --annotationtype {params.anno} {input.de} {input.map} {input.stats} "
        "2> {log} | gzip > {output}"


rule attach_tx_stats:
    input:
        de=join(DIR_RES, "003_diffexp/{contrast}.tx.diffexp.tsv.gz"),
        map=join(DIR_RES, "tx_gene_map.txt"),
        stats=join(DIR_RES, "002_expr/tx.tpm.stats.txt.gz")
    output:
        join(DIR_RES, "003_diffexp/{contrast}.tx.diffexp.stats.tsv.gz")
    log:
        join(DIR_LOGS, "attach_tx_symbol_{contrast}.log")
    params:
        script=join(DIR_SCRIPTS, "join.py"),
        anno=CFG_GTF_SOURCE
    shell:
        "python {params.script} --annotationtype {params.anno} --tx {input.de} {input.map} {input.stats} "
        "2> {log} | gzip > {output}"
        
## ===================================================================
## gsea
rule gsea_msigdb_symbols:
    input:
        join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.stats.tsv.gz")
    output:
        join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.all.tsv.gz"),
        join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.all.tsv.gz"),
        join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.selected.tsv.gz"),
        join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.selected.tsv.gz")
    log:
        join(DIR_LOGS, "gsea_msigdb_symbols/{contrast}.log")
    conda:
        join(DIR_ENVS, "clusterprofiler.yaml")
    params:
        script=join(DIR_SCRIPTS, "gsea_msigdb_symbols.R")
    shell:
        "zcat {input} | cut -f 1-9 | Rscript --vanilla --slave {params.script} "
        "{CFG_ENRICH_GCOL} {CFG_ENRICH_GMT} {CFG_GSEA_NPERMS} "
        "{output[0]} {output[1]} {output[2]} {output[3]} "
        "2> {log}"

        
## the old gsea script 
## rule gsea_msigdb:
##     input:
##         join(DIR_RES, "003_diffexp/{contrast}.genes.diffexp.tsv.gz")
##     output:
##         join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.all.tsv.gz"),
##         join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.all.tsv.gz"),
##         join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-up.selected.tsv.gz"),
##         join(DIR_RES, "004_gsea/{contrast}.diffexp.gsea-dn.selected.tsv.gz")
##     log:
##         join(DIR_LOGS, "gsea_msigdb/{contrast}.log")
##     conda:
##         join(DIR_ENVS, "clusterprofiler.yaml")
##     params:
##         script=join(DIR_SCRIPTS, "gsea_msigdb.R")
##     shell:
##         "zcat {input} | Rscript --vanilla --slave {params.script} "
##         "{CFG_ENRICH_GCOL} {CFG_ENRICH_ID} {CFG_ENRICH_ORG} {CFG_ENRICH_GMT} "
##         "{CFG_GSEA_NPERMS} {output[0]} {output[1]} {output[2]} {output[3]} "
##         "2> {log}"
